from flask import request
from app import login_manager
from flask_login import LoginManager
from app.models import User


@login_manager.user_loader
def user_loader(id):
    """
    Check user's session
    """
    return User.query.get(id)


def authenticate(username, password):
    user = User.query.filter_by(username=username).first()
    authenticated = False
    if password == user.password:
        authenticated = True
    return user, authenticated
    