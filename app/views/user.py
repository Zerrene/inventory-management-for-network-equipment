from flask import render_template, abort, redirect, request, url_for, Blueprint
import flask_login
from app import app, db, login_manager
from app.models import User, Role, Project, ProjectMember
from app.forms import UserForm


user = Blueprint('user', __name__)

@user.route('/', methods=['GET'])
@flask_login.login_required
def index():
    users = User.query.all()
    user = flask_login.current_user
    return render_template('user/index.html', user=user, users=users, page='user')


@user.route('/profile', methods=['GET'])
@flask_login.login_required
def profile(id=None):
    user = User.query.filter_by(id=flask_login.current_user.id).first()
    user_current = flask_login.current_user
    projects = Project.query.all()
    if not user:
        abort(404)
    return render_template('user/member/detail.html', user_current=user_current, user=user, project=projects, page='user')


@user.route('/<id>', methods=['GET'])
@flask_login.login_required
def get(id=None):
    user = User.query.filter_by(id=id).first()
    projects = ProjectMember.query.filter_by(user_id=id).all()
    user_current = flask_login.current_user
    if not user:
        abort(404)
    print "User id: " +user.id
    print "User Current id:" + user_current.id
    return render_template('user/member/detail.html', user_current=user_current, user=user, projects=projects, page='user')


@user.route('/register', methods=['GET', 'POST'])
@flask_login.login_required
def create():
    form = UserForm(request.form)
    choices = [(c.id, c.name) for c in Role.query.order_by(Role.name).all()]
    form.role.choices = choices
    roles = Role.query.all()
    if request.method == 'GET':
        return render_template('user/member/add.html', form=form, roles=roles, page='user')

    if request.method == 'POST':
        role = Role.query.filter_by(id=form.role.data).first()
        user = User(id=form.id.data, 
                    gender=form.gender.data,
                    firstname=form.firstname.data,
                    lastname=form.lastname.data,
                    nickname='',
                    telephone=form.telephone.data,
                    email=form.email.data,
                    username=form.username.data,
                    password=form.password.data,
                    role=role,
                    creator=flask_login.current_user)
        db.session.add(user)
        db.session.commit()
    return redirect(url_for('user.index'))


@user.route('/<id>/edit', methods=['GET', 'POST'])
@flask_login.login_required
def edit(id=None):
    user = User.query.filter_by(id=id).first()
    if not user:
        abort(404)
    if request.method == 'GET':
        form = UserForm(obj=user)
        choices = [(c.id, c.name) for c in Role.query.order_by(Role.name).all()]
        form.role.choices = choices
        form.role.process_data(user.role.id)
        roles = Role.query.all()
        return render_template('user/member/edit.html', user=user, roles=roles, form=form, page='user')
    
    form = UserForm(request.form)
    if request.method == 'POST':
        user.gender = form.gender.data
        user.firstname = form.firstname.data
        user.lastname = form.lastname.data
        user.nickname = form.nickname.data
        user.telephone = form.telephone.data
        user.email = form.email.data
        if user.role.id == 1:
            user.role_id = form.role.data
        db.session.merge(user)
        db.session.commit()
    return redirect(url_for('user.index'))

# @user.route('/profile', methods=['GET', 'POST'])
# def profile(id=None):
#     user = User.query.filter_by(id=id).first()
#     if request.method == 'GET':
#         form = UserForm(obj=user)
#         return render_template('user/edit.html', user=user, form=form)
    
#     form = UserForm(request.form)
#     if request.method == 'POST':
#         id=form.id.data
#         gender=form.gender.data
#         firstname=form.firstname.data
#         lastname=form.lastname.data
#         nickname=form.nickname.data
#         telephone=form.telephone.data
#         email=form.email.data
#         username=form.username.data
#         password=form.password.data
#         db.session.merge(user)
#         db.session.commit
#     return redirect(url_for('user.index'))

@user.route('/role', methods=['GET', 'POST'])
def role():
    return render_template('user/role.html', page='user')

@user.route('/role/add', methods=['GET', 'POST'])
def add_role():
    return render_template('user/add_role.html', page='user')


