from flask import render_template, abort, redirect, request, url_for, Blueprint
import flask_login
from app import app, db, login_manager, upload_file
from app.models import Item, Project, Order ,ItemFile, ItemLocation, IndexWarrantyType
from app.forms import LocationForm, UploadFileForm, ItemForm


item = Blueprint('item', __name__)


#--- Detail ---
@item.route('/<id>/', methods=['GET'])
@flask_login.login_required
def item_detail(id=None):
    item = Item.query.filter_by(serial_number=id).first()
    if not item:
        abort(404)    
    return render_template('item/detail.html', item=item, page='material')


@item.route('/<id>/edit', methods=['GET', 'POST'])
@flask_login.login_required
def item_detail_edit(id=None):
    item = Item.query.filter_by(serial_number=id).first()
    if request.method == 'GET':
        form = ItemForm(obj=item)
        choices = [(c.id, c.name) for c in IndexWarrantyType.query.order_by(IndexWarrantyType.name).all()]
        form.warranty_type.choices = choices
        form.warranty_type.process_data(item.warranty_type.id)
        warranty_types = IndexWarrantyType.query.all()
        return render_template('item/edit.html', item=item, warranty_types=warranty_types, form=form, page='material')
    
    form = ItemForm(request.form)
    if request.method == 'POST':
        item.box_number = form.box_number.data
        item.name = form.name.data
        item.warranty_type_id = form.warranty_type.data
        item.warranty_expire = form.warranty_expire.data
        item.sap = form.sap.data
        db.session.merge(item)
        db.session.commit()
    return redirect(url_for('item.item_detail', id=item.serial_number))


# --- Project ---
@item.route('/<id>/project', methods=['GET'])
@flask_login.login_required
def item_project(id=None):
    item = Item.query.filter_by(serial_number=id).first()
    if not item:
        abort(404)
    return render_template('item/project.html', item=item, page='material')


# --- Location ---
@item.route('/<id>/location', methods=['GET'])
@flask_login.login_required
def item_location(id=None):
    item = Item.query.filter_by(serial_number=id).first()
    locations = ItemLocation.query.filter_by(item_serial_number=id).all()
    if not item:
        abort(404)
    return render_template('item/location.html', item=item, locations=locations, page='material')


# --- File ---
@item.route('/<id>/file', methods=['GET'])
@flask_login.login_required
def item_files(id=None):
    item = Item.query.filter_by(serial_number=id).first()
    if not item:
        abort(404)
    files = item.files.all()
    return render_template('item/file.html', item=item, files=files, page='material')



@item.route('/<id>/file/upload', methods=['GET', 'POST'])
@flask_login.login_required
def item_file_upload(id=None):
    item = Item.query.filter_by(serial_number=id).first()
    form = UploadFileForm(request.form)
    if request.method == 'GET':
        return render_template('item/upload.html', form=form, item=item, page='material')

    if request.method == 'POST':
        file = request.files.get('afile')
        description = request.form.get('description')
        original_name, hash_name = upload_file(file)
        itemfile = ItemFile(name=original_name,
                            object_name=hash_name,
                            type=form.type.data,
                            description=form.description.data,
                            serial=item,
                            uploader=flask_login.current_user)
        db.session.add(itemfile)
        db.session.commit()
    return redirect(url_for('item.item_files', id=item.serial_number))