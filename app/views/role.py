from flask import render_template, abort, redirect, request, url_for, Blueprint
import flask_login
from app import app, db, login_manager
from app.models import User, Role
from app.forms import RoleForm


role = Blueprint('role', __name__)

@role.route('/', methods=['GET'])
@flask_login.login_required
def index():
    roles = Role.query.all()
    return render_template('user/role/index.html', roles=roles, page='user')

@role.route('/<id>', methods=['GET', 'POST'])
@flask_login.login_required
def edit(id=None):
    role = Role.query.filter_by(id=id).first()
    if not role:
        abort(404)
    
    if request.method == 'GET':
        form = RoleForm(obj=role)
        return render_template('user/role/edit.html', role=role, form=form, page='user')
    
    form = RoleForm(request.form)
    if request.method == 'POST':
        role.name = form.name.data
        role.description = form.description.data
        db.session.merge(role)
        db.session.commit()
    return redirect(url_for('role.index'))

@role.route('/create', methods=['GET', 'POST'])
@flask_login.login_required
def create():
    form = RoleForm(request.form)
    if request.method == 'GET':
        return render_template('user/role/add.html', form=form, page='user')

    if request.method == 'POST':
        role = Role(name=form.name.data,
                    description=form.description.data)
        db.session.add(role)
        db.session.commit()
    return redirect(url_for('role.index'))