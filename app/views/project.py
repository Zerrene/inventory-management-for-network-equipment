from flask import render_template, abort, redirect, request, url_for, Blueprint, abort
import flask_login
from app import app, db, login_manager, upload_file
from app.models import Project, ProjectFile, ProjectMember, ProjectLocation, Customer, Order, Item,\
                    ItemLocation, ItemStatus, MaterialType, ProjectFileType, IndexWarrantyType, Material, Location
from app.forms import ProjectForm, OrderForm, CustomerForm, UploadForm, ItemForm
import datetime

project = Blueprint('project', __name__)


@project.route('/', methods=['GET'])
@flask_login.login_required
def index():
    projects = Project.query.all()
    user = flask_login.current_user
    return render_template('project/index.html', user=user, projects=projects, page='project')


# --- Create ---
@project.route('/create', methods=['GET', 'POST'])
@flask_login.login_required
def create():
    form = ProjectForm(request.form)
    if request.method == 'GET':
        return render_template('project/add.html', form=form, page='project')
        
    if request.method == 'POST':
        project = Project(id=form.id.data, 
                        name=form.name.data, 
                        description=form.description.data, 
                        goal=form.goal.data,
                        progress=form.progress.data, 
                        agreement=form.agreement.data,
                        status=form.status.data,
                        creator=flask_login.current_user,
                        updated_time=str(datetime.datetime.utcnow()))
        db.session.add(project)
        db.session.commit()
    return redirect(url_for('project.index'))


# --- Detail ---
@project.route('/<id>', methods=['GET'])
@flask_login.login_required
def get(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    return render_template('project/detail/index.html', project=project, page='project')


@project.route('/<id>/detail/edit', methods=['GET', 'POST'])
@flask_login.login_required
def detail_edit(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    
    if request.method == 'GET':
        form = ProjectForm(obj=project)
        return render_template('project/detail/edit.html', project=project, form=form, page='project')
    
    form = ProjectForm(request.form)
    if request.method == 'POST':
        project.name = form.name.data
        project.description = form.description.data
        project.updated_time = str(datetime.datetime.utcnow())
        db.session.merge(project)
        db.session.commit()
    return redirect(url_for('project.get', id=project.id))


# --- Order ---
@project.route('/<id>/order', methods=['GET'])
@flask_login.login_required
def order(id=None):
    project = Project.query.filter_by(id=id).first()
    orders = Order.query.filter_by(project=project).all()
    return render_template('project/order/index.html', project=project, orders=orders, page='project')


@project.route('/<id>/order/add', methods=['GET', 'POST'])
@flask_login.login_required
def order_add(id=None):
    project = Project.query.filter_by(id=id).first()
    form = OrderForm(request.form)
    if request.method == 'GET':
        return render_template('project/order/add.html', form=form, page='project')

    if request.method == 'POST':
        order = Order(number=form.number.data,
                    project=project,
                    description=form.description.data,
                    bill_of_material=form.bill_of_material.data,
                    vendor=form.vendor.data,
                    creator=flask_login.current_user)
        db.session.add(order)
        db.session.commit()
    return redirect(url_for('project.order', id=project.id))


@project.route('/<id>/order/edit', methods=['GET', 'POST'])
@flask_login.login_required
def order_edit(id=None):
    return render_template('project/order/edit.html', project=project, page='project')


# --- Customer ---
@project.route('/<id>/customer', methods=['GET'])
@flask_login.login_required
def customer(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    customers = Customer.query.filter_by(project=project).all()
    return render_template('project/customer/index.html', project=project, customers=customers, page='project')


@project.route('/<id>/customer/add', methods=['GET', 'POST'])
@flask_login.login_required
def customer_add(id=None):
    project = Project.query.filter_by(id=id).first()
    form = CustomerForm(request.form)
    if request.method == 'GET':
        return render_template('project/customer/add.html', form=form, page='project')

    if request.method == 'POST':
        customer = Customer(firstname=form.firstname.data,
                            lastname=form.lastname.data,
                            location=form.location.data,
                            telephone=form.telephone.data,
                            email=form.email.data,
                            description=form.description.data,
                            project=project,
                            creator=flask_login.current_user)
        
        db.session.add(customer)
        db.session.commit()
    return redirect(url_for('project.customer', id=project.id))


# --- Member ---
@project.route('/<id>/member', methods=['GET'])
@flask_login.login_required
def member(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    members = ProjectMember.query.filter_by(project=project).all()
    return render_template('project/member/index.html', project=project, members=members, page='project')


@project.route('/<id>/member/manage', methods=['GET'])
@flask_login.login_required
def member_manage(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    return render_template('project/member/manage.html', project=project, page='project')


@project.route('/<id>/member/add', methods=['GET'])
@flask_login.login_required
def member_add(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    return render_template('project/member/add.html', project=project, page='project')


# --- Item ---
@project.route('/<id>/item', methods=['GET'])
@flask_login.login_required
def item(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    orders = project.orders.all()
    materials = MaterialType.query.all()
    itemlocation = ItemLocation.query.all()
    return render_template('project/item/index.html', project=project, orders=orders, materials=materials, itemlocation=itemlocation, page='project')


@project.route('/<id>/item/add', methods=['GET', 'POST'])
@flask_login.login_required
def item_add(id=None):
    project = Project.query.filter_by(id=id).first()
    form = ItemForm(request.form)
    if not project:
        abort(404)
    
    choices = [(b.number, b.model) for b in Material.query.order_by(Material.model).all()]
    form.material.choices = choices
    materials = Material.query.all()
    
    choices = [(c.id, c.number) for c in Order.query.filter_by(project_id=id).all()]
    form.order.choices = choices
    orders = Order.query.all()

    choices = [(d.id, d.name) for d in IndexWarrantyType.query.order_by(IndexWarrantyType.name).all()]
    form.warranty_type.choices = choices
    warranty_types = IndexWarrantyType.query.all()

    # choices = [(e.id, e.name) for e in ProjectLocation.query.filter_by(project=id).all()]
    choices = [(e.id, e.name) for e in Location.query.order_by(Location.name).all()]
    form.location.choices = choices
    locations = IndexWarrantyType.query.all()
    if request.method == 'GET':
        return render_template('project/item/add.html', form=form, materials=materials, orders=orders, warranty_types=warranty_types, locations=locations, page='project')

    if request.method == 'POST':
        material = Material.query.filter_by(number=form.material.data).first()
        order = Order.query.filter_by(id=form.order.data).first()
        warranty_type = IndexWarrantyType.query.filter_by(id=form.warranty_type.data).first()
        location = Location.query.filter_by(id=form.location.data).first()
        item = Item(serial_number=form.serial_number.data,
                    box_number=form.box_number.data,
                    name=form.name.data,
                    warranty_expire=form.warranty_expire.data,
                    sap=form.sap.data,
                    material=material,
                    order=order,
                    warranty_type=warranty_type)
        
        itemlocation = ItemLocation(location=location,
                                    item=item,
                                    status=form.status.data,
                                    equipment=form.equipment.data,
                                    creator=flask_login.current_user)
        print '<location: {}, item: {}>'.format(location.id, item.serial_number)
        db.session.add(item)
        db.session.add(itemlocation)
        db.session.commit()
    return redirect(url_for('project.item', id=project.id))

# @project.route('/<id>/item/setting', methods=['GET'])
# @flask_login.login_required
# def item_setting(id=None):
#     project = Project.query.filter_by(id=id).first()
#     if not project:
#         abort(404)
#     items = Order.query.filter_by(project=project).all()
#     return render_template('project/item/setting.html', project=project, items=items, page='project')


# --- Location ---
@project.route('/<id>/location', methods=['GET'])
@flask_login.login_required
def location(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    locations = ProjectLocation.query.filter_by(project=project).all()
    return render_template('project/location/index.html', project=project, locations=locations, page='project')


@project.route('/<id>/location/detail', methods=['GET'])
@flask_login.login_required
def location_detail(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    return render_template('project/location/detail.html', project=project, page='project')


@project.route('/<id>/location/add', methods=['GET'])
@flask_login.login_required
def location_add(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    return render_template('project/location/add.html', project=project, page='project')


# --- File ---
@project.route('/<id>/file', methods=['GET', 'POST'])
@flask_login.login_required
def files(id=None):
    project = Project.query.filter_by(id=id).first()
    if not project:
        abort(404)
    files = project.files.all()
    return render_template('project/file/index.html', project=project, files=files, page='project')


@project.route('/<id>/file/upload', methods=['GET', 'POST'])
@flask_login.login_required
def file_upload(id=None):
    project = Project.query.filter_by(id=id).first()
    form = UploadForm(request.form)
    if request.method == 'GET':
        return render_template('project/file/upload.html', form=form, page='project')

    if request.method == 'POST':
        file = request.files.get('afile')
        description = request.form.get('description')
        original_name, hash_name = upload_file(file)
        projectfile = ProjectFile(name=original_name,
                                object_name=hash_name,
                                type=form.type.data,
                                description=form.description.data,
                                project=project,
                                uploader=flask_login.current_user)
        db.session.add(projectfile)
        db.session.commit()
    return redirect(url_for('project.files', id=project.id))