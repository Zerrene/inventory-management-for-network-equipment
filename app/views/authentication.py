from flask import Flask, abort, render_template, redirect, request, url_for, Blueprint
from app import app
from app.models import User
from app.auth import authenticate
from app.forms import LoginForm
from flask_login import login_user, logout_user


authentication = Blueprint('authentication', __name__)

@authentication.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if request.method == 'GET':
        return render_template('login.html', form=form)

    if form.validate():
        username = request.form['username']
        password = request.form['password']
        user, authenticated = authenticate(username, password)
        if user:
            if authenticated:
                login_user(user)
                return redirect(url_for('project.index'))
            else:
                return render_template('login.html', form=form)
    return render_template('login.html', form=form)


@authentication.route('/logout', methods=['GET'])
def logout():
    logout_user()
    return redirect(url_for('authentication.login'))
