from flask import render_template, redirect, url_for
from app import app, login_manager

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

@login_manager.unauthorized_handler
def unauthorized_handler():
    return redirect(url_for('authentication.login'))