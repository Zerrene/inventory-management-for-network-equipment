from flask import render_template, abort, redirect, request, url_for, Blueprint
from app import app, db
from app.models import Tag


tag = Blueprint('tag', __name__)


@tag.route('/create', methods=['POST'])
def create():
    if request.method == 'POST':
        tag = Tag(request.form['name'])
        db.session.add(tag)
        db.session.commit()
        return 'ok'
