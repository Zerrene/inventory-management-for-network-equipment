from flask import render_template, abort, redirect, request, url_for, Blueprint
import flask_login
from app import app, db, login_manager
from app.models import Location, LocationType, LocationRegion
from app.forms import LocationForm


location = Blueprint('location', __name__)


@location.route('/', methods=['GET'])
@flask_login.login_required
def index():
    locations = Location.query.all()
    return render_template('location/index.html', locations=locations, page='location')


@location.route('/<id>/', methods=['GET', 'POST'])
@flask_login.login_required
def detail(id=None):
    location = Location.query.filter_by(id=id).first()
    return render_template('location/detail.html', location=location, page='location')


@location.route('/add', methods=['GET', 'POST'])
def create():
    form = LocationForm(request.form)
    if request.method == 'GET':
        return render_template('location/add.html', form=form, page='location')
    if request.method == 'POST':
        location = Location(id = form.id.data,
                            name = form.name.data,
                            type = form.type.data,
                            region = form.region.data,
                            province = form.province.data,
                            district = form.district.data,
                            zipcode = form.zipcode.data,
                            description = form.description.data,
                            comment = form.comment.data,
                            creator=flask_login.current_user)
        db.session.add(location)
        db.session.commit()
    return redirect(url_for('location.index'))


@location.route('/<id>/edit', methods=['GET', 'POST'])
@flask_login.login_required
def edit(id=None):
    location = Location.query.filter_by(id=id).first()
    if not location:
        abort(404)

    if request.method == 'GET':
        form = LocationForm(obj=location)
        return render_template('location/edit.html', location=location, form=form, page='location')
    
    form = LocationForm(request.form)
    if request.method == 'POST':
        location.name = form.name.data
        location.type = form.type.data
        location.region = form.region.data
        location.province = form.province.data
        location.district = form.district.data
        location.zipcode = form.zipcode.data
        location.description = form.description.data
        location.comment = form.comment.data
        db.session.merge(location)
        db.session.commit()
    return redirect(url_for('location.detail', id=location.id))



