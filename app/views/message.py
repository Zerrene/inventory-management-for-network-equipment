from flask import render_template, abort, redirect, request, url_for, Blueprint
import flask_login
from app import app, db, login_manager
from app.models import User,Message
from app.forms import MessageForm


message = Blueprint('message', __name__)

@message.route('/', methods=['GET'])
@flask_login.login_required
def index():
    messages = Message.query.all()
    return render_template('message/index.html', messages=messages, page='request')

@message.route('/form', methods=['GET', 'POST'])
@flask_login.login_required
def request_form():
    form = MessageForm(request.form)
    if request.method == 'GET':
        return render_template('message/form.html', form=form, page='request')

    if request.method == 'POST':
        message = Message(project_id=form.project_id.data,
                        text=form.text.data,
                        reply=form.reply.data,
                        creator=flask_login.current_user)
        db.session.add(message)
        db.session.commit()
    return redirect(url_for('message.index'))

@message.route('/<id>', methods=['GET', 'POST'])
@flask_login.login_required
def request_edit(id=None):
    user = flask_login.current_user
    message = Message.query.filter_by(id=id).first()
    if not message:
        abort(404)
    
    if request.method == 'GET':
        form = MessageForm(obj=message)
        return render_template('message/detail.html', message=message, user=user, form=form, page='request')
        
    
    form = MessageForm(request.form)
    if request.method == 'POST':
        message.reply = form.reply.data
        db.session.merge(message)
        db.session.commit()
    return redirect(url_for('message.index'))
