from flask import render_template, abort, redirect, request, url_for, Blueprint
import flask_login
from app import app, db, login_manager
from app.models import Material, MaterialType, Brand, OperatingSystem
from app.forms import MaterialForm, MaterialTypeForm, MaterialOSForm, MaterialBrandForm


material = Blueprint('material', __name__)


@material.route('/', methods=['GET'])
@flask_login.login_required
def index():
    materials = Material.query.all()
    return render_template('material/index.html', materials=materials, page='material')


@material.route('/<id>', methods=['GET'])
@flask_login.login_required
def get(id=None):
    material = Material.query.filter_by(number=id).first()
    if not material:
        abort(404)
    return render_template('material/detail.html', material=material, page='material')


@material.route('/create', methods=['GET', 'POST'])
@flask_login.login_required
def create():
    form = MaterialForm(request.form)
    choices = [(c.id, c.name) for c in MaterialType.query.order_by(MaterialType.name).all()]
    form.type.choices = choices
    types = MaterialType.query.all()

    choices = [(d.id, d.name) for d in Brand.query.order_by(Brand.name).all()]
    form.brand.choices = choices
    brands = Brand.query.all()
    if request.method == 'GET':
        return render_template('material/add.html', form=form, types=types, brands=brands, page='material')

    if request.method == 'POST':
        brand = Brand.query.filter_by(id=form.brand.data).first()
        type = MaterialType.query.filter_by(id=form.type.data).first()
        material = Material(number=form.number.data, 
                            brand=brand,
                            series=form.series.data,
                            model=form.model.data,
                            serial_length=form.serial_length.data,
                            os=form.os.data,
                            type=type,
                            description=form.description.data,
                            creator=flask_login.current_user)
        db.session.add(material)
        db.session.commit()
    return redirect(url_for('material.index'))


@material.route('/<id>/edit', methods=['GET', 'POST'])
@flask_login.login_required
def edit(id=None):
    form = MaterialForm(request.form)
    material = Material.query.filter_by(number=id).first()
    if not material:
        abort(404)
    if request.method == 'GET':
        form = MaterialForm(obj=material)
        return render_template('material/edit.html', material=material, form=form, page='material')
    
    if request.method == 'POST':
        # material.number = form.number.data
        # material.brand = form.brand.data
        material.series = form.series.data
        material.model = form.model.data
        material.serial_length = form.serial_length.data
        material.description = form.description.data
        db.session.merge(material)
        db.session.commit()
    return redirect(url_for('material.index'))


@material.route('/type', methods=['GET'])
@flask_login.login_required
def type_index():
    types = MaterialType.query.all()
    return render_template('material/type/index.html', types=types, page='material')


@material.route('/type/add', methods=['GET', 'POST'])
@flask_login.login_required
def type_add():
    form = MaterialTypeForm(request.form)
    if request.method == 'GET':
        return render_template('material/type/add.html', form=form, page='material')

    if request.method == 'POST':
        materialtype = MaterialType(
                    name=form.name.data,
                    description=form.description.data,
                    font_color=form.font_color.data,
                    cell_color=form.cell_color.data)
        db.session.add(materialtype)
        db.session.commit()
    return redirect(url_for('material.type_index'))


@material.route('/type/<id>', methods=['GET', 'POST'])
@flask_login.login_required
def type_edit(id=None):
    materialtype = MaterialType.query.filter_by(id=id).first()
    if not materialtype:
        abort(404)
    
    if request.method == 'GET':
        form = MaterialTypeForm(obj=materialtype)
        return render_template('material/type/edit.html', materialtype=materialtype, form=form, page='material')
    
    form = MaterialTypeForm(request.form)
    if request.method == 'POST':
        materialtype.name = form.name.data
        materialtype.description = form.description.data
        materialtype.font_color = form.font_color.data
        materialtype.cell_color = form.cell_color.data
        db.session.merge(materialtype)
        db.session.commit()
    return redirect(url_for('material.type_index'))

# --- Operation System ---
@material.route('/os', methods=['GET'])
@flask_login.login_required
def os_index():
    multi_os = OperatingSystem.query.all()
    return render_template('material/os/index.html', multi_os=multi_os, page='material')


@material.route('/os/add', methods=['GET', 'POST'])
@flask_login.login_required
def os_add():
    form = MaterialOSForm(request.form)
    choices = [(d.id, d.name) for d in Brand.query.order_by(Brand.name).all()]
    form.brand.choices = choices
    brands = Brand.query.all()
    if request.method == 'GET':
        return render_template('material/os/add.html', form=form, page='material')

    if request.method == 'POST':
        brand = Brand.query.filter_by(id=form.brand.data).first()
        os = OperatingSystem(name=form.name.data,
                            brand=brand,
                            description=form.description.data)
        db.session.add(os)
        db.session.commit()
    return redirect(url_for('material.os_index'))


@material.route('/os/<id>', methods=['GET', 'POST'])
@flask_login.login_required
def os_edit(id=None):
    os = OperatingSystem.query.filter_by(id=id).first()
    if not os:
        abort(404)
    
    if request.method == 'GET':
        form = MaterialOSForm(obj=os)
        return render_template('material/os/edit.html', os=os, form=form, page='material')
    
    form = MaterialOSForm(request.form)
    if request.method == 'POST':
        os.name = form.name.data
        os.description = form.description.data
        db.session.merge(os)
        db.session.commit()
    return redirect(url_for('material.os_index'))


# --- Brand ---
@material.route('/brand', methods=['GET'])
@flask_login.login_required
def brand_index():
    brands = Brand.query.all()
    return render_template('material/brand/index.html', brands=brands, page='material')


@material.route('/brand/add', methods=['GET', 'POST'])
@flask_login.login_required
def brand_add():
    form = MaterialBrandForm(request.form)
    if request.method == 'GET':
        return render_template('material/brand/add.html', form=form, page='material')

    if request.method == 'POST':
        brand = Brand(name=form.name.data,
                    description=form.description.data)
        db.session.add(brand)
        db.session.commit()
    return redirect(url_for('material.brand_index'))


@material.route('/brand/<id>', methods=['GET', 'POST'])
@flask_login.login_required
def brand_edit(id=None):
    brand = Brand.query.filter_by(id=id).first()
    if not brand:
        abort(404)
    
    if request.method == 'GET':
        form = MaterialBrandForm(obj=brand)
        return render_template('material/brand/edit.html', brand=brand, form=form, page='material')
    
    form = MaterialBrandForm(request.form)
    if request.method == 'POST':
        brand.name = form.name.data
        brand.description = form.description.data
        db.session.merge(brand)
        db.session.commit()
    return redirect(url_for('material.brand_index'))
