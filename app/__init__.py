from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_bootstrap import Bootstrap
from flask_login import LoginManager
from .user import User
from werkzeug.utils import secure_filename
import os

app = Flask(__name__)
app.config.from_object('app.config')
app.secret_key = 'PqBa4QPQTFLcXnytKTD4qSBV9xt4av7TMVXFWxQ6'

db = SQLAlchemy(app)

login_manager = LoginManager()
login_manager.init_app(app)
Bootstrap(app)


# flask-uploads
UPLOAD_FOLDER = 'C:\\Users\\Zerrene\\workspaces\\untitled_project\\files'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


def gen_time():
    return str(datetime.datetime.now()).replace(' ', '_').replace('.', '_').replace(':', '_')


def upload_file(file):
    filename = secure_filename(file.filename)
    hashname = '{}_{}'.format(gen_time(), filename)
    path = os.path.join(UPLOAD_FOLDER, hashname)
    file.save(path)
    return filename, hashname
    

from models import *
from .auth import *


from flask import send_from_directory

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(UPLOAD_FOLDER, filename)

from werkzeug import SharedDataMiddleware
app.add_url_rule('/uploads/<filename>', 'uploaded_file',
                 build_only=True)
app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
    '/uploads':  UPLOAD_FOLDER
})

from app.views.authentication import authentication
from app.views.project import project
from app.views.location import location
from app.views.material import material
from app.views.user import user
from app.views.tag import tag
from app.views.role import role
from app.views.message import message
from app.views.item import item

app.register_blueprint(authentication)
app.register_blueprint(project, url_prefix="/project")
app.register_blueprint(location, url_prefix="/location")
app.register_blueprint(material, url_prefix="/material")
app.register_blueprint(user, url_prefix="/user")
app.register_blueprint(tag, url_prefix="/tag")
app.register_blueprint(role, url_prefix="/role")
app.register_blueprint(message, url_prefix="/message")
app.register_blueprint(item, url_prefix="/item")