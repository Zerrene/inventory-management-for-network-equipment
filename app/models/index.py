from app import db

class IndexWarrantyType(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	name = db.Column(db.String(45), nullable=False)
	description = db.Column(db.Text)
	font_color = db.Column(db.String(8))
	cell_color = db.Column(db.String(8))

	items = db.relationship('Item', lazy='dynamic')

	def __init__(self, name, description, font_color, cell_color):
		self.name = name
		self.description = description
		self.font_color = font_color
		self.cell_color = cell_color

# class IndexStatus(db.Model):
# 	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
# 	name = db.Column(db.String(45), nullable=False)
# 	description = db.Column(db.Text)
# 	font_color = db.Column(db.String(8))
# 	cell_color = db.Column(db.String(8))
	
# 	def __init__(self, name, description, font_color, cell_color):
# 		self.name = name
# 		self.description = description
# 		self.font_color = font_color
# 		self.cell_color = cell_color