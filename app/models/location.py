from app import db
import enum

class LocationType(enum.Enum):
	PROVINCENODE = 'Province Node'
	WAREHOUSE = 'Warehouse'


class LocationRegion(enum.Enum):
	CENTRAL = 'Central'
	EAST = 'East'
	NORTHEAST = 'North East'
	WEST = 'West'
	def __repr__(self):
		return '<Region: name=%r, value=%r>' % (name, value)


class Location(db.Model):
	id = db.Column(db.String(20), primary_key=True, nullable=False)
	name = db.Column(db.String(30), nullable=False)
	type = db.Column(db.Enum(LocationType), nullable=False)
	region = db.Column(db.Enum(LocationRegion), nullable=False)
	province = db.Column(db.String(50))
	district = db.Column(db.String(50))
	zipcode = db.Column(db.String(5))
	description = db.Column(db.Text)
	comment = db.Column(db.Text)

	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	creator = db.relationship('User')
	projects = db.relationship("ProjectLocation", lazy='dynamic')

	def __init__(self, id, name, creator, type=LocationType.PROVINCENODE, region=None, province=None, district=None, zipcode=None, description=None, comment=None):
		 self.id = id
		 self.name = name
		 self.type = type
		 self.region = region
		 self.province = province
		 self.district = district
		 self.zipcode = zipcode
		 self.description = description
		 self.comment = comment
		 self.creator = creator

	def __repr__(self):
		return '<Location id={}, name={}, type={}>'.format(self.id, self.name, self.type.value)