from app import db
import datetime

class Order(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	number = db.Column(db.String(15), nullable=False)
	bill_of_material = db.Column(db.String(15), nullable=False)
	vendor = db.Column(db.String(50), nullable=False)
	description = db.Column(db.Text)

	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	creator = db.relationship('User')
	created_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

	project_id = db.Column(db.String(15), db.ForeignKey('project.id'), nullable=False)
	project = db.relationship("Project")

	items = db.relationship("Item", lazy='dynamic')

	def __init__(self, number, description, bill_of_material, vendor, project, creator):
		self.number = number
		self.project = project
		self.description = description
		self.bill_of_material = bill_of_material
		self.vendor = vendor
		self.creator = creator
	