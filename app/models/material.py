import datetime
from app import db

material_tags = db.Table('material_tag',
	db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'), primary_key=True, nullable=False),
	db.Column('material_number', db.String(20), db.ForeignKey('material.number'), primary_key=True, nullable=False)
	)

class Material(db.Model):
	number = db.Column(db.String(20), primary_key=True, nullable=False)
	series = db.Column(db.String(45), nullable=False)
	model = db.Column(db.String(20), nullable=False)
	serial_length = db.Column(db.Integer, nullable=False)
	description = db.Column(db.Text)


	os_id = db.Column(db.Integer, db.ForeignKey('operating_system.id'), default=None)
	os = db.relationship('OperatingSystem')
	brand_id = db.Column(db.Integer, db.ForeignKey('brand.id'), nullable=False)
	brand = db.relationship('Brand')
	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	creator = db.relationship('User')
	created_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

	items = db.relationship('Item', lazy='dynamic')
	tags = db.relationship('Tag', secondary=material_tags,
			backref=db.backref('materials'), lazy='dynamic')

	type_id = db.Column(db.Integer, db.ForeignKey('material_type.id'), nullable=False)
	type = db.relationship('MaterialType')

	def __init__(self, number, brand, series, model, serial_length, type, os=None, description=None, creator=None):
		self.number = number
		self.brand = brand
		self.series = series
		self.model = model
		self.serial_length = serial_length
		self.os = os
		self.brand = brand
		self.type = type
		self.description = description
		if creator is not None:
			self.creator = creator
			

class MaterialType(db.Model):
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	name = db.Column(db.String(30), nullable=False)
	description = db.Column(db.Text)
	font_color = db.Column(db.String(20), default='#000000')
	cell_color = db.Column(db.String(20), default='#FFFFFF')
	
	materials = db.relationship('Material', lazy='dynamic')

	def __init__(self, name, description, font_color=None, cell_color=None):
		self.name = name
		self.description = description
		self.font_color = font_color
		self.cell_color = cell_color

	# def __repr__(self):
	# 	return "<MaterialType: id={}, name={}, description={}, font_color={}, cell_color={}>".format(
	# 		self.id, self.name, self.description, self.font_color, self.cell_color)
	def __repr__(self):
		return '<Type: id=%r, name=%r>' % (self.id, self.name)


class Brand(db.Model):
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	name = db.Column(db.String(30), nullable=False)
	description = db.Column(db.Text)

	materials = db.relationship('Material', lazy='dynamic')

	os = db.relationship('OperatingSystem', lazy='dynamic')
	def __init__(self, name, description=None):
		self.name = name
		self.description = description

	def __repr__(self):
		return '<Brand: id=%r, name=%r>' % (self.id, self.name)


class OperatingSystem(db.Model):
	id = db.Column(db.Integer, autoincrement=True, primary_key=True)
	name = db.Column(db.String(30), nullable=False)
	description = db.Column(db.Text)


	brand_id = db.Column(db.Integer, db.ForeignKey('brand.id'))
	brand = db.relationship('Brand')

	materials = db.relationship('Material', lazy='dynamic')
# 	brand = db.relationship('Brand')

	def __init__(self, name, brand, description=None):
		self.name = name
		self.description = description
		self.brand = brand

	def __repr__(self):
		return '<OS: id=%r, name=%r, brand=%r>' % (self.id, self.name, self.brand)

	