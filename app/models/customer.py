from app import db
import datetime


class Customer(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	firstname = db.Column(db.String(45), nullable=False)
	lastname = db.Column(db.String(45), nullable=False)
	location = db.Column(db.Text, nullable=False)
	telephone = db.Column(db.String(10), nullable=False)
	email = db.Column(db.String(60), nullable=False)
	description = db.Column(db.Text)
	creator_id = db.Column(db.String(15), db.ForeignKey('user.id'), nullable=False)
	creator = db.relationship('User')
	created_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
	
	project_id = db.Column(db.String(15), db.ForeignKey('project.id'), nullable=False)
	project = db.relationship('Project')

	def __init__(self, firstname, lastname, location, telephone, email, project, creator, description=None):
		self.firstname = firstname
		self.lastname = lastname
		self.location = location
		self.telephone = telephone
		self.email = email
		self.project = project
		self.creator = creator
		self.description = description

	@property
	def fullname(self):
		return "{} {}".format(self.firstname, self.lastname)

	def __repr__(self):
		return '<Customer: id={}, firstname={}, project_id={}>'.format(self.id, self.firstname, self.project_id)
