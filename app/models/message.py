from app import db
import datetime

class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    project_id = db.Column(db.String(15))
    text = db.Column(db.Text, nullable=False)
    reply = db.Column(db.Text)
    created_time = db.Column(db.DateTime , nullable=False, default=datetime.datetime.utcnow)
    creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)

    creator = db.relationship("User", foreign_keys=[creator_id])

    def __init__(self, project_id, text, reply, creator):
        self.project_id = project_id
        self.text = text
        self.reply = reply
        self.creator = creator