from app import db
import enum, datetime
from sqlalchemy import desc


class ItemStatus(enum.Enum):
	SHIPPING = 'Shipping'
	STAGING  = 'Staging' 
	DELIVERING  = 'Delivering' 
	INSTALLING  = 'Installing' 
	INSTALLED  = 'Installed' 
	MAINTENANCE  = 'Maintenance'
	REPLACED  = 'Replaced' 
	SPARE  = 'Spare' 
	CLAIM  = 'Claim'

class ItemLocation(db.Model):
	__tablename__ = 'item_location'
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	location_id = db.Column(db.String(20), db.ForeignKey('location.id'))
	item_serial_number = db.Column(db.String(40), db.ForeignKey('item.serial_number'))
	status = db.Column(db.Enum(ItemStatus))
	equipment = db.Column(db.String(40))
	commend = db.Column(db.Text)
	updated_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)

	location = db.relationship('Location')
	item = db.relationship('Item')
	creator = db.relationship('User')

	def __init__(self, location, item, status, equipment, creator):
		self.location = location
		self.item = item
		self.status = status
		self.equipment = equipment
		self.creator = creator

	@property
	def cell_color(self):
		cell_colors = {
			ItemStatus.SHIPPING: '#FFBC9E',
			ItemStatus.STAGING: '#F9F1A4',
			ItemStatus.DELIVERING: '#F5C4FC',
			ItemStatus.INSTALLING: '#8BDFF9',
			ItemStatus.INSTALLED: '#91FFB1',
			ItemStatus.MAINTENANCE: '#C6FFFE',
			ItemStatus.REPLACED: '#C19EFF',
			ItemStatus.SPARE: '#DDDDDD',
			ItemStatus.CLAIM: '#FF96A2',
		}
		return cell_colors[self.status]


class Item(db.Model):
	serial_number = db.Column(db.String(40), primary_key=True, nullable=False)
	box_number = db.Column(db.String(20), nullable=False)
	warranty_expire = db.Column(db.Date, nullable=False)
	name = db.Column(db.String(50))
	sap = db.Column(db.String(45))
	
	warranty_type_id = db.Column(db.Integer, db.ForeignKey('index_warranty_type.id'), nullable=False)
	order_id = db.Column(db.Integer, db.ForeignKey('order.id'), nullable=False)
	material_number = db.Column(db.String(20), db.ForeignKey('material.number'), primary_key=True, nullable=False)

	warranty_type = db.relationship('IndexWarrantyType')
	order = db.relationship('Order')
	material = db.relationship('Material')
	location = db.relationship('ItemLocation')
	files = db.relationship('ItemFile', lazy='dynamic')
	ip = db.relationship('ItemIP')

	def __init__(self, serial_number, box_number, name, warranty_expire, warranty_type, order, material, sap=None):
		self.serial_number = serial_number
		self.box_number = box_number
		self.name = name
		self.warranty_expire = warranty_expire
		self.warranty_type = warranty_type
		self.order = order
		self.material = material
		self.sap = sap

	@property
	def latest_item_location(self):
		return ItemLocation.query.filter_by(item=self).order_by(desc(ItemLocation.updated_time)).first()
		


class ItemIP(db.Model):
	__tablename__ = 'item_ip'
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	ip_address = db.Column(db.String(45))
	port = db.Column(db.String(45))

	item_serial_number = db.Column(db.String(40), db.ForeignKey('item.serial_number'), nullable=False)
	item = db.relationship('Item')

	def __init__(self, ip_address, port, item):
		self.ip_address = ip_address
		self.port = port
		self.item = item


class ItemFileType(enum.Enum):
	LOG = 'Log'
	CONFIGURATION = 'Configuration'
	INVENTORY = 'Inventory'
	PAT = 'PAT'
	OTHER = 'Other'


class ItemFile(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	type = db.Column(db.Enum(ItemFileType), nullable=False, default=ItemFileType.OTHER)
	name = db.Column(db.String(50))
	object_name = db.Column(db.String(300))
	object_url = db.Column(db.String(150))
	uploaded_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

	uploader_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	uploader = db.relationship('User')
	serial_number = db.Column(db.String(20), db.ForeignKey('item.serial_number'), nullable=False)
	serial = db.relationship('Item')

	def __init__(self, name, type, description, object_name, serial, uploader, object_url=None):
		self.name = name
		self.type = type
		self.description = description
		self.object_name = object_name
		self.serial = serial
		self.uploader = uploader
		self.object_url = object_url