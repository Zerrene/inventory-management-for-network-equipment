import enum
import datetime
from app import db


project_tags = db.Table('project_tag',
	db.Column('tag_id', db.Integer, db.ForeignKey('tag.id'), primary_key=True, nullable=False),
	db.Column('project_id', db.String(15), db.ForeignKey('project.id'), primary_key=True, nullable=False)
)


class ProjectMemberRole(enum.Enum):
	PROJECTMANAGER = 'Project Manager'
	ENGINEER = 'Engineer'
	SALES = 'Sales'


class ProjectMemberStatus(enum.Enum):
	ACTIVE = 'Active'
	DEACTIVE = 'Deactive'


class ProjectMember(db.Model):
	__tablename__ = 'project_member'
	project_id = db.Column(db.String(15), db.ForeignKey('project.id'), primary_key=True)
	user_id = db.Column(db.String(10), db.ForeignKey('user.id'), primary_key=True)
	role = db.Column(db.Enum(ProjectMemberRole), nullable=False, default=ProjectMemberRole.ENGINEER)
	status = db.Column(db.Enum(ProjectMemberStatus), nullable=False, default=ProjectMemberStatus.DEACTIVE)
	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)

	project = db.relationship("Project")
	user = db.relationship("User", foreign_keys=[user_id])
	creator = db.relationship("User", foreign_keys=[creator_id])
	created_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

	def __init__(self, project, user, role, status, created_time, creator):
		self.project = project
		self.user = user
		self.role = role
		self.status = status
		self.created_time = created_time
		self.creator = creator


class ProjectLocation(db.Model):
	__tablename__ = 'project_location'
	project_id = db.Column(db.String(15), db.ForeignKey('project.id'), primary_key=True)
	project = db.relationship("Project")
	location_id = db.Column(db.String(20), db.ForeignKey('location.id'), primary_key=True)
	location = db.relationship("Location")
	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	creator = db.relationship('User')
	created_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)

	def __init__(self, project, location, creator):
		self.project = project
		self.location = location
		self.creator = creator


class ProjectStatus(enum.Enum):
	INPROGRESS = 'Inprogress'
	MAINTAINANCE = 'Maintainance'
	COMPLETED = 'Completed'


class Project(db.Model):
	id = db.Column(db.String(15), primary_key=True, nullable=False)
	name = db.Column(db.String(60), nullable=False)
	description = db.Column(db.Text, nullable=False)
	goal = db.Column(db.Text, nullable=False)
	created_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
	updated_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
	status = db.Column(db.Enum(ProjectStatus), nullable=False)
	progress = db.Column(db.Integer)
	agreement = db.Column(db.Text, nullable=False)
	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	creator = db.relationship('User')

	tags = db.relationship('Tag', secondary=project_tags, backref=db.backref('projects', lazy='dynamic'))

	members = db.relationship('ProjectMember', lazy='dynamic')

	locations = db.relationship("ProjectLocation", lazy='dynamic')
	files = db.relationship('ProjectFile', lazy='dynamic')
	customers = db.relationship('Customer', lazy='dynamic')
	orders = db.relationship('Order', lazy='dynamic')

	def __init__(self, id, name, description, goal, status, progress, agreement, creator, updated_time=datetime.datetime.utcnow()):
		self.id = id
		self.name = name
		self.description = description
		self.goal = goal
		self.status = status
		self.progress = progress
		self.agreement = agreement
		self.creator = creator
		self.updated_time = updated_time

	@property
	def label_style(self):
		label_styles = {
			ProjectStatus.COMPLETED: 'label label-success',
			ProjectStatus.INPROGRESS: 'label label-warning',
			ProjectStatus.MAINTAINANCE: 'label label-info'
		}
		return label_styles[self.status]


class ProjectFileType(enum.Enum):
	TOPOLOGY = 'Topology'
	AGREEMENT = 'Agreement'
	PRODUCTORDER = 'Product Order'
	BILLOFMATERIAL = 'Bill of Material'
	OTHER = 'Other'

class ProjectFile(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	type = db.Column(db.Enum(ProjectFileType), nullable=False, default=ProjectFileType.OTHER)
	name = db.Column(db.String(50), nullable=False)
	object_name = db.Column(db.String(150))
	object_url = db.Column(db.String(150))
	description = db.Column(db.Text)
	uploaded_time = db.Column(db.DateTime, nullable=False, default=datetime.datetime.utcnow)
	
	uploader_id = db.Column(db.String(10), db.ForeignKey('user.id'), nullable=False)
	uploader = db.relationship('User')
	project_id = db.Column(db.String(15), db.ForeignKey('project.id'), nullable=False)
	project = db.relationship('Project', backref='roles')

	def __init__(self, name, type, description, object_name, project, uploader, object_url=None):
		self.name = name
		self.type = type
		self.description = description
		self.object_name = object_name
		self.project = project
		self.uploader = uploader
		self.object_url = object_url