from app import db

class Tag(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True, nullable=False)
	name = db.Column(db.String(45), nullable=False)

	def __init__(self, name):
		self.name = name


# class TagMaterial(db.Model):
# 	id = db.Column(db.Integer, nullable=False)
# 	tags_id = db.Column(db.Integer, primary_key=True, nullable=False)
# 	material_number = db.Column(db.String(20), primary_key=True, nullable=False)


# class TagProject(db.Model):
# 	id = db.Column(db.Integer, nullable=False)
# 	tags_id = db.Column(db.Integer, primary_key=True, nullable=False)
# 	project_id = db.Column(db.String(15), primary_key=True, nullable=False)