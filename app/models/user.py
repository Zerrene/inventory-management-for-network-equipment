from flask_login import UserMixin
from app import db
import datetime


class User(db.Model, UserMixin):
	id = db.Column(db.String(10), primary_key=True, nullable=False)
	gender = db.Column(db.String(6), nullable=False)
	firstname = db.Column(db.String(20), nullable=False)
	lastname = db.Column(db.String(20), nullable=False)
	nickname = db.Column(db.String(10))
	telephone = db.Column(db.String(10))
	email = db.Column(db.String(60), unique=True, nullable=False)
	created_time = db.Column(db.DateTime, default=datetime.datetime.utcnow)
	username = db.Column(db.String(25),unique=True, nullable=False)
	password = db.Column(db.String(45), nullable=False)
	
	creator_id = db.Column(db.String(10), db.ForeignKey('user.id'))
	creator = db.relationship("User", remote_side=[id])
	role_id = db.Column(db.Integer, db.ForeignKey('role.id'), default=2)
	role = db.relationship("Role")

	def __init__(self, id, gender, firstname, lastname, nickname, telephone, email, username, password, role=None, creator=None):
		self.id = id
		self.gender = gender
		self.firstname = firstname
		self.lastname = lastname
		self.nickname = nickname
		self.telephone = telephone
		self.email = email
		self.username = username
		self.password = password
		self.role = role
		if creator is not None:
			self.creator = creator

	@property
	def fullname(self):
		if self.gender == 'Male':
			return '{} {} {}'.format('Mr.', self.firstname, self.lastname)
		else:
			return '{} {} {}'.format('Ms.', self.firstname, self.lastname)

	def __repr__(self):
		return '<User: id=%r, fullname=%r, role=%r>' % (self.id, self.fullname, self.role.id)


class Role(db.Model):
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	name = db.Column(db.String(45), unique=True, nullable=False)
	description = db.Column(db.Text)
	created_time = db.Column(db.DateTime, default=datetime.datetime.utcnow)

	def __init__(self, name, description=None):
		self.name = name
		self.description = description

	def __repr__(self):
		return '<Role: id=%r, name=%r>' % (self.id, self.name)
