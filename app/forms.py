from flask_wtf import FlaskForm
from wtforms import *
from wtforms_components import ColorField
from wtforms.validators import DataRequired, InputRequired

from models import LocationRegion, LocationType, ProjectStatus, ProjectFileType, ItemFileType, ItemStatus


class LoginForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class RoleForm(FlaskForm):
	name = StringField('name', [InputRequired("Please enter your name.")])
	description = TextAreaField('description', render_kw={"rows": 6})


class UserForm(FlaskForm):
	id = StringField('id', validators=[DataRequired()])
	gender = RadioField('gendor', choices=[('Male','Male'),('Female','Female')])
	firstname = StringField('firstname', validators=[DataRequired()])
	lastname = StringField('lastname', validators=[DataRequired()])
	nickname = StringField('nickname')
	telephone = StringField('telephone', validators=[DataRequired()])
	email = StringField('email', validators=[DataRequired()])
	username = StringField('username', validators=[DataRequired()])
	password = PasswordField('password', validators=[DataRequired()])
	role = SelectField('role', choices=[], coerce=int)


class MaterialForm(FlaskForm):
	number = StringField('number', validators=[DataRequired()])
	brand = SelectField('brand', choices=[], coerce=int)
	series = StringField('series', validators=[DataRequired()])
	model = StringField('model', validators=[DataRequired()])
	type = SelectField('type', choices=[], coerce=int)
	serial_length = StringField('serial_length', validators=[DataRequired()])
	os = SelectField('os', choices=[], coerce=int)
	description = TextAreaField('description', render_kw={"rows": 6})


class MaterialOSForm(FlaskForm):
	name = StringField('name', validators=[DataRequired()])
	brand = SelectField('brand', choices=[], coerce=int)
	description = TextAreaField('description', render_kw={"rows": 6})


class MaterialBrandForm(FlaskForm):
	name = StringField('name', validators=[DataRequired()])
	description = TextAreaField('description', render_kw={"rows": 6})


class DeviceForm(FlaskForm):
	sellorder = StringField('sellorder', validators=[DataRequired()])
	productorder = StringField('productorder', validators=[DataRequired()])
	materialnumber = StringField('materialnumber', validators=[DataRequired()])
	status = StringField('status', validators=[DataRequired()])
	location = StringField('location', validators=[DataRequired()])
	tag = StringField('tag', validators=[DataRequired()])


class ItemForm(FlaskForm):
	serial_number = StringField('serial_number', validators=[DataRequired()])
	box_number = StringField('box_number', validators=[DataRequired()])
	name = StringField('name', validators=[DataRequired()])
	warranty_expire = DateField('warranty_expire', validators=[DataRequired()])
	sap = StringField('sap', validators=[DataRequired()])
	warranty_type = SelectField('warranty_type', choices=[], coerce=int)
	order = SelectField('order', choices=[], coerce=int)
	material = SelectField('material', choices=[], coerce=int)

	location = SelectField('location', choices=[], coerce=str)
	status = SelectField('status', choices=[(e.name, e.value) for e in ItemStatus])
	equipment = StringField('equipment')


class LocationForm(FlaskForm):
	id = StringField('id', validators=[DataRequired()])
	name = StringField('name', validators=[DataRequired()])
	type = SelectField('type', choices=[(e.name, e.value) for e in LocationType])
	region = SelectField('region', choices=[(e.name, e.value) for e in LocationRegion])
	province = StringField('province')
	district = StringField('district')
	zipcode = StringField('zipcode')
	description = TextAreaField('description', render_kw={"rows": 6})
	comment = TextAreaField('comment', render_kw={"rows": 6})


class DetailForm(FlaskForm):
	projectname = StringField('name', validators=[DataRequired()])
	description = StringField('description', validators=[DataRequired()])
	objective = StringField('objective', validators=[DataRequired()])
	agreement = StringField('agreement', validators=[DataRequired()])
	tag = StringField('tag', validators=[DataRequired()])


class ProjectForm(FlaskForm):
	id = StringField('id', validators=[DataRequired()])
	name = StringField('name', validators=[DataRequired()])
	description = TextAreaField('description', render_kw={"rows": 6})
	goal = TextField('goal', validators=[DataRequired()])
	progress = IntegerField('progress')
	agreement = TextAreaField('agreement', render_kw={"rows": 6})
	status = SelectField('status', choices=[(e.name, e.value) for e in ProjectStatus])


class OrderForm(FlaskForm):
	number = StringField('number')
	description = TextAreaField('description', render_kw={"rows": 6})
	bill_of_material = StringField('bill_of_material')
	vendor = StringField('vendor')


class CustomerForm(FlaskForm):
	firstname = StringField('firstname', validators=[DataRequired()])
	lastname = StringField('lastname', validators=[DataRequired()])
	location = TextAreaField('location', render_kw={"rows": 6})
	telephone = StringField('telephone', validators=[DataRequired()])
	email = StringField('email', validators=[DataRequired()])
	description = TextAreaField('description', render_kw={"rows": 6})


class MaterialTypeForm(FlaskForm):
	name = StringField('name')
	description = TextAreaField('description', render_kw={"rows": 6})
	font_color = StringField('font_color')
	cell_color = StringField('cell_color')


class MessageForm(FlaskForm):
	project_id = StringField('project_id')
	text = TextAreaField('text', render_kw={"rows": 6})
	reply = TextAreaField('description', render_kw={"rows": 6})


class UploadForm(FlaskForm):
	name = StringField('name')
	type = SelectField('type', choices=[(e.name, e.value) for e in ProjectFileType])
	object_name = StringField('object_name')
	object_url = StringField('object_url')
	description = TextAreaField('description', render_kw={"rows": 6})

class UploadFileForm(FlaskForm):
	name = StringField('name')
	type = SelectField('type', choices=[(e.name, e.value) for e in ItemFileType])
	object_name = StringField('object_name')
	object_url = StringField('object_url')
	description = TextAreaField('description', render_kw={"rows": 6})


# class ChangePassword(Form):
#     password = PasswordField('New Password', [InputRequired(), EqualTo('confirm', message='Passwords must match')])
#     confirm  = PasswordField('Repeat Password')