#!flask/bin/python
from app import app
app.jinja_env.auto_reload = True
app.config['TEMPLATES_AUTO_RELOAD'] = True
app.run(host='0.0.0.0', debug=True, use_reloader=True)