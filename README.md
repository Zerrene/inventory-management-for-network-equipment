# Inventory Management for Network Equipment

## Setup Project

1.ดาวน์โหลด Python เวอร์ชั่น 2.7 จาก [ที่นี่](https://www.python.org/downloads/)


2.ทำการสร้าง Schema ใน MySQL Workbench โดยใช้ชื่อว่า **inventorymanagement**

![Screenshot_46.png](https://bitbucket.org/repo/Ggg78yd/images/2617596742-Screenshot_46.png)


3.ดาวน์โหลด Module ที่ระบบใช้โดยการเปิด CMD แล้วไปที่ Root path ของโปรเจคแล้วพิมพ์คำสั่ง

```bash
pip install -r requirements.txt
```


4.ทำการสร้างข้อมูลตัวอย่างได้โดยการเปิด CMD แล้วไปที่ Root path ของโปรเจคแล้วพิมพ์คำสั่ง

```bash
python migrate.py
```


## Run as Localhost

1.เปิดเซิร์ฟเวอร์จำลอง MySQL (localhost)

![Screenshot_44.png](https://bitbucket.org/repo/Ggg78yd/images/2561858591-Screenshot_44.png)


2.เปิด CMD แล้วไปที่ Root path ของโปรเจคแล้วพิมพ์คำสั่ง **python run.py**

![Screenshot_45.png](https://bitbucket.org/repo/Ggg78yd/images/1747009001-Screenshot_45.png)


## Database Migration

สร้าง Migration repository

```bash
python manager.py db init
```

ทำการ Generate migration script

```bash
python manager.py db migrate
```

Generate ตาราง

```bash
python manager.py db upgrade
```

## Author

- Attachai Chaiwanna