import enum


class LocationRegion(enum.Enum):
	CENTRAL = 'Central'
	EAST = 'East'
	NORTHEAST = 'North East'
	WEST = 'West'

	def __repr__(self):
		return '<Region: name=%r, value=%r>' % (name, value)


print [(e.name, e.value) for e in LocationRegion]